import { axiosClient } from "./axiosClient";

const orderApi = {
  async confirm(id) {
    const url = `/order/confirm/${id}`;
    axiosClient.put(url);
  },

  async getAll(filter) {
    var url = `/order/orders?pageNum=${filter.pageNum}&pageSize=${filter.pageSize}`;
    const response = await axiosClient.get(url);
    return {
      data: response.data.data,
    };
  },

  async checkout(data) {
    const url = "/order/checkout";
    return await axiosClient.post(url, data);
  },
  async getMyOrder(userId, pageNumer, pagesize) {
    const url = `/order/myorder/${userId}?pageNum=${pageNumer}&pageSize=${pagesize}`;
    const response = await axiosClient.get(url);
    return response.data;
  },
  async getByEmail(email) {
    const url = `order/email/${email}`;
    const response = await axiosClient.get(url);
    return response.data;
  },
  async get(id) {
    const url = `/order/${id}`;
    return axiosClient.get(url);
  },
};
export default orderApi;
