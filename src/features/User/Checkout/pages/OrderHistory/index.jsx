import { Button } from '@material-ui/core';
import { nanoid } from 'nanoid';
import React, { useEffect, useState } from 'react';
import orderApi from '../../../../../api/orderApi';
import Hero from '../../../../../components/headers/Hero';


OrderHistory.propTypes = {

};

function OrderHistory(props) {
    const [orders, setorders] = useState([]);
    const user = JSON.parse(localStorage.getItem('user'));
    const getOrders = async () => {
        const ordersFromApi = await orderApi.getMyOrder(user.userId, 0, 10);
        setorders(ordersFromApi.data.content);
    }
    const [orderDetails, setOrderDetails] = useState([]);
    const handleClose = () => {
        setOrderDetails([]);
    }
    const handleOpen = (order) => {
        setOrderDetails(order.orderedProduct)
    }
    useEffect(() => {
        getOrders();
    }, [])
    return (
        <div>

            <Hero title={orders.length !== 0 ? 'Histories' : 'Bạn chưa từng đặt đơn hàng nào trước đây'} />
            {orders.length !== 0 &&
                <div className="cart-box-main">
                    <div className="container" >
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="table-main table-responsive">
                                    <form>
                                        <table className="table" key={nanoid()}>

                                            <thead>
                                                <tr>
                                                    <th>{'Order Id'}</th>
                                                    <th>{orderDetails.length !== 0 ? 'Product Image' : "Status"}</th>
                                                    <th>{orderDetails.length !== 0 ? 'Quantity' : 'Create Date'}</th>
                                                    {orderDetails.length !== 0 && <th>Price</th>}
                                                    {orderDetails.length === 0 && <th>Details</th>}
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {orderDetails.length !== 0 ?
                                                    (orderDetails.map((orderDetail) => (
                                                        <tr key={orderDetail.orderId}>
                                                            <td className="name-pr">
                                                                {orderDetail.id.orderId}
                                                            </td>
                                                            <td className="name-pr">
                                                                <img src={orderDetail.product.imgURL} style={{ width: '180px', height: '100px' }} />
                                                            </td>
                                                            {/* <NumberInputFeild /> */}
                                                            <td className="name-pr">
                                                                <p>{orderDetail.quantity}</p>
                                                            </td>
                                                            <td className="name-pr">
                                                                <p>{orderDetail.price}</p>
                                                            </td>
                                                        </tr>

                                                    ))) :
                                                    (orders.map((order) => (
                                                        <tr key={order.id}>
                                                            <td className="name-pr">
                                                                {order.id}
                                                            </td>
                                                            <td className="name-pr">
                                                                {order.status === false ?
                                                                    <p> Chưa xác nhận</p>
                                                                    :
                                                                    <p> Đã xác nhận</p>
                                                                }
                                                            </td>
                                                            {/* <NumberInputFeild /> */}
                                                            <td className="name-pr">
                                                                <p>{order.createDate}</p>
                                                            </td>
                                                            <td className="name-pr" style={{ border: 'none' }}>
                                                                <p><Button className="btn hvr-hover" onClick={() => handleOpen(order)} style={{ color: 'white', width: '100%' }}> View</Button></p>
                                                            </td>

                                                        </tr>
                                                    )))}
                                                {orderDetails.length !== 0 && <tr ><Button className="btn hvr-hover" onClick={handleClose} style={{ color: 'white', width: '100px' }}> Back</Button></tr>}
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            }
        </div>
    );
}

export default OrderHistory;