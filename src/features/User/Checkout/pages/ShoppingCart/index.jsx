import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import orderApi from '../../../../../api/orderApi';
import Hero from '../../../../../components/headers/Hero';
import CartInPage from '../../components/CartInPage';
import { update } from '../../counterSlice'
import { useSnackbar } from 'notistack';
import { PayPalButton } from 'react-paypal-button-v2';

ShoppingCart.propTypes = {

};

function ShoppingCart(props) {
  if (JSON.parse(localStorage.getItem('cart')) !== null) {
    var cartFromLocalStorage = JSON.parse(localStorage.getItem('cart'));
  } else {
    cartFromLocalStorage = [];
  }

  const dispatch = useDispatch();

  const [cart, setCart] = useState(cartFromLocalStorage);

  const changeValueinCart = (id, value) => {

    const index = cart.findIndex((bike) => bike.id === id);
    const newQuantity = Number(value);
    const newBike = { ...cart[index], quantity: newQuantity };
    const tmpCart = [...cart];
    tmpCart[index] = newBike;

    const totalBikes = tmpCart.reduce((total, { quantity }) => total + quantity, 0);
    localStorage.setItem('cart', JSON.stringify(tmpCart));
    setCart(tmpCart)
    dispatch(update(totalBikes));
  }

  const removeFromCart = (id) => {
    const index = cart.findIndex((bike) => bike.id === id);
    const tmpCart = [...cart];

    tmpCart.splice(index, 1);
    const totalBikes = tmpCart.reduce((total, { quantity }) => total + quantity, 0);
    localStorage.setItem('cart', JSON.stringify(tmpCart));
    setCart(tmpCart);
    dispatch(update(totalBikes));
  }


  const totalPrice = cart.reduce((total, { quantity, price }) => total + price * quantity, 0);

  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const handleCheckoutClick = () => {
    const orders = [];
    cart.forEach(c => {
      orders.push({ productId: c.id, quantity: c.quantity })
    });
    try {
      orderApi.checkout(orders);
      history.replace('/bikes');
      localStorage.removeItem('cart');
      dispatch(update(0));
      enqueueSnackbar('Hệ thống đã nhận được đơn hàng', { variant: 'success' });
    } catch (error) {

    }
  }

  return (
    <>
      <Hero title={cart.length !== 0 ? 'Shopping cart' : 'Không có hàng trong giỏ của bạn'} />

      {cart.length !== 0 &&
        <div className="cart-box-main">
          <div className="container" >
            <CartInPage cart={cart} change={changeValueinCart} remove={removeFromCart} />
            <div className="row my-5 ">
              <div className="col-lg-5 col-sm-6">
                <div className="coupon-box">
                  <div className="input-group input-group-sm">
                    <input className="form-control" placeholder="Enter your coupon code" aria-label="Coupon code" type="text" />
                    <div className="input-group-append">
                      <button className="btn btn-theme" type="button">Nhập mã giảm giá</button>
                    </div>
                  </div>
                </div>
                <PayPalButton
                  amount={totalPrice - 10 + 2}
                  // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
                  onSuccess={(details, data) => {
                    alert(`Chúc mừng ${details.payer.name.given_name} đã thực hiện giao dịch thành công với Paypal, liên hệ với tư vấn viên bán hàng để biết thêm chi tiết`);
                    // OPTIONAL: Call your server to save the transaction
                    try {
                      const orders = [];
                      cart.forEach(c => {
                        orders.push({ productId: c.id, quantity: c.quantity })
                      });
                      orderApi.checkout(orders);
                      localStorage.removeItem('cart');
                      dispatch(update(0));
                      enqueueSnackbar("Hệ thống đã nhận được đơn hàng", { variant: 'success' });
                      history.replace('/bikes');
                    } catch (error) {

                    }

                  }}
                />
              </div>

              <div className="col-lg-4 col-sm-12">
                <div className="order-box">
                  <h3>Tổng kết đơn hàng</h3>
                  <div className="d-flex">
                    <h4>Giá trị ban đầu</h4>
                    <div className="ml-auto font-weight-bold"> $ {totalPrice} </div>
                  </div>
                  <div className="d-flex">
                    <h4>Giảm giá</h4>
                    <div className="ml-auto font-weight-bold"> $ 0 </div>
                  </div>
                  <hr className="my-1" />
                  <div className="d-flex">
                    <h4>Giảm theo mã</h4>
                    <div className="ml-auto font-weight-bold"> $ 10 </div>
                  </div>
                  <div className="d-flex">
                    <h4>Thuế VAT</h4>
                    <div className="ml-auto font-weight-bold"> $ 2 </div>
                  </div>
                  <div className="d-flex">
                    <h4>Tiền ship</h4>
                    <div className="ml-auto font-weight-bold"> Free </div>
                  </div>
                  <hr />
                  <div className="d-flex gr-total">
                    <h5>Giá trị đơn hàng</h5>
                    <div className="ml-auto h5"> $ {totalPrice - 10 + 2} </div>
                  </div>
                  <hr /> </div>
              </div>
              <div className="col-9 d-flex shopping-box"><a onClick={handleCheckoutClick} className="ml-auto btn hvr-hover">Xác nhận</a> </div>
            </div>
          </div>
        </div>
      }
    </>
  );
}

export default ShoppingCart;