import { unwrapResult } from '@reduxjs/toolkit';
import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { login } from '../../userSlice';
import LoginForm from '../LoginForm';




Login.propTypes = {
    closeDialog: PropTypes.func,
};

function Login(props) {

    const dispatch = useDispatch();
    const { enqueueSnackbar } = useSnackbar();
    const history = useHistory();


    const handleSubmit = async (values) => {
        try {
            //----
            const action = login(values);
            const resultAction = await dispatch(action);
            const response = unwrapResult(resultAction);
            //
            const user = response.data.data
            if (user.role.roleName === "ADMIN") {
                history.push("/admin/accounts");
            }
            if (user.role.roleName === "SALER") {
                history.push("/admin/orders");
            }
            if (user.role.roleName === "MODIFIER") {
                history.push("/admin/bikes");
            }
            
            //close dialog
            const { closeDialog } = props;
            if (closeDialog) {
                closeDialog();
            }
        } catch (error) {
            enqueueSnackbar(error.message, { variant: 'error' });

        }
    };
    return (
        <div>
            <LoginForm onSubmit={handleSubmit} />
        </div>
    );
}

export default Login;