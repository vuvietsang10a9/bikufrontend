import React from 'react';
import OrderDetailTable from '../../components/OrderDetailTable';

OrderDetailPage.propTypes = {
    
};
function OrderDetailPage() {

    return (
        <>
            <div id="page-wrapper">
                <div id="page-inner">
                    {/* /. ROW  */}
                    <div className="row">
                        <div className="col-md-12">
                            {/* Advanced Tables */}

                            <div className="panel panel-default ">
                                <div className="panel-body">
                                    <div className="table-responsive">
                                        <OrderDetailTable/>
                                    </div>
                                    {/*End Advanced Tables */}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default OrderDetailPage;




