import { useSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React from 'react';
import accountApi from '../../../../../api/accountApi';
import UpdateForm from '../../components/UpdateForm';

UpdateAccount.propTypes = {
    handleClose: {}
};

UpdateAccount.DefaultProp = {
    handleClose: PropTypes.func.isRequired,
};

function UpdateAccount({ handleUpdated, account }) {

    const { enqueueSnackbar } = useSnackbar()
    const user = JSON.parse(localStorage.getItem('user'));
    const handleSubmit = async (values) => {

        const newValues = { ...values, role: values.roletmp }
        try {
            await accountApi.updateByAdmin(user.userId, newValues);
            handleUpdated();
            enqueueSnackbar('Register successfully', { variant: 'success' });
        } catch (error) {
            enqueueSnackbar(error, { variant: 'error' });
        }
    }
    return (
        <div>
            <UpdateForm updateAccount={account} onSubmit={handleSubmit} />
        </div>
    );
}

export default UpdateAccount;